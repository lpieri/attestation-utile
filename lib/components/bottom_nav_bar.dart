import 'package:attestation_utile/constants.dart';
import 'package:flutter/material.dart';

class BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: kDefaultPadding * 2,
        right: kDefaultPadding * 2,
        bottom: kDefaultPadding,
        top: kDefaultPadding,
      ),
      height: 80,
      decoration: BoxDecoration(
        color: kPrimaryColor,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -10),
            blurRadius: 35,
            color: kPrimaryColor.withOpacity(0.38),
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: Icon(
              Icons.add_box_rounded,
              color: Colors.white,
              size: 24.0,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(
              Icons.article_outlined,
              color: Colors.white,
              size: 24.0,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(
              Icons.account_circle_outlined,
              color: Colors.white,
              size: 24.0,
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
