import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF1A237E);  // indigo 900
const kBackgroundColor = Color(0xFFFFFFFF); // indigo 50
const kTextColor = Color(0xFFFFFFFF);
const kPlaceholderColor = Color(0xFF757575); // grey 600
const kDefaultPadding = 20.0;
const kServerUrl = 'http://192.168.1.15:8000/';
const kCertificateDirectory = '/sdcard/Download/';
