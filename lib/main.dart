import 'package:attestation_utile/constants.dart';
import 'package:attestation_utile/screens/certificate_generator/certificate_generator_screen.dart';
import 'package:attestation_utile/screens/user_preferences/user_preferences_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/user_preferences': (context) => UserPreferencesScreen()
      },
      debugShowCheckedModeBanner: false,
      title: 'Attestation utile',
      theme: ThemeData(
        scaffoldBackgroundColor: kBackgroundColor,
        primaryColor: kPrimaryColor,
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CertificateGeneratorScreen(),
    );
  }
}
