class User {
  final String _firstname;
  final String _lastname;
  final DateTime _birthdate;
  final String _birthplace;
  final String _address;
  final String _city;
  final int _zipcode;

  User(this._firstname, this._lastname, this._birthdate, this._birthplace,
      this._address, this._city, this._zipcode);

  User.fromJson(Map<String, dynamic> json)
      : _firstname = json['firstname'],
        _lastname = json['lastname'],
        _birthdate = DateTime.parse(json['birthdate']),
        _birthplace = json['birthplace'],
        _address = json['address'],
        _city = json['city'],
        _zipcode = int.parse(json['zipcode']);

  Map<String, dynamic> toJson() => {
        'firstname': _firstname,
        'lastname': _lastname,
        'birthdate': _birthdate.toString().substring(0, 10),
        'birthplace': _birthplace,
        'address': _address,
        'city': _city,
        'zipcode': _zipcode.toString(),
      };
}
