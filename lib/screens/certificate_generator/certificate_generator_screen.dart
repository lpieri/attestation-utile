import 'dart:convert';
import 'dart:io';

import 'package:attestation_utile/constants.dart';
import 'package:attestation_utile/models/user.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CertificateGeneratorScreen extends StatefulWidget {
  @override
  _CertificateGeneratorScreenState createState() =>
      _CertificateGeneratorScreenState();
}

class _CertificateGeneratorScreenState
    extends State<CertificateGeneratorScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Motif de déplacements'),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ReasonButtonWidget(
                onTap: () => generatePdf('1', 'Déplacement professionnel'),
                title: 'Déplacement professionnel',
                subtitle:
                    "Déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle ou le lieu d’enseignement et de formation, déplacements professionnels ne pouvant être différés.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('2', 'Déplacement médical'),
                title: 'Déplacement médical',
                subtitle:
                    "Déplacements pour des consultations et soins ne pouvant être assurés à distance et ne pouvant être différés ou pour l’achat de produits de santé.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('3', 'Déplacement impérieux'),
                title: 'Déplacement impérieux',
                subtitle:
                    "Déplacements pour motif familial impérieux, pour l’assistance aux personnes vulnérables ou précaires ou pour la garde d’enfants.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('4', 'Déplacement pour personnes handicapées'),
                title: 'Déplacement pour personnes handicapées',
                subtitle:
                    "Déplacements des personnes en situation de handicap et de leur accompagnant.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('5', 'Déplacement administratif'),
                title: 'Déplacement administratif',
                subtitle:
                    "Déplacements pour répondre à une convocation judiciaire ou administrative.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('6', 'Déplacement sur demande des autorités'),
                title: 'Déplacement sur demande des autorités',
                subtitle:
                    "Déplacements pour participer à des missions d’intérêt général sur demande de l’autorité administrative.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('7', 'Déplacement sur de longues distances'),
                title: 'Déplacement sur de longues distances',
                subtitle:
                    "Déplacements liés à des transits ferroviaires ou aériens pour des déplacements de longues distances.",
              ),
              ReasonButtonWidget(
                onTap: () => generatePdf('8', 'Déplacement pour les animaux de compagnie'),
                title: 'Déplacement pour les animaux de compagnie',
                subtitle:
                    "Déplacements brefs, dans un rayon maximal d’un kilomètre autour du domicile pour les besoins des animaux de compagnie.",
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<User> getUser() async {
    final SharedPreferences prefs = await _prefs;
    final json = prefs.getString('user');

    if (json == null) {
      return null;
    }

    return User.fromJson(jsonDecode(json));
  }

  void generatePdf(String authorizationReasonIndex, String authorizationReasonLabel) async {
    final now = new DateTime.now().toString();
    final user = await getUser();

    if (user == null) {
      Navigator.pushNamed(context, '/user_preferences');
    }

    final file = File(kCertificateDirectory + UniqueKey().toString() + '-' + authorizationReasonLabel + '.pdf');

    try {
      http.Response response = await http.post(
        kServerUrl,
        body: {
          ...user.toJson(),
          'authorization_date': now.substring(0, 10),
          'authorization_time': now.substring(11, 19),
          'authorization_reason': authorizationReasonIndex
        },
      );

      if (response.statusCode == 200) {
        await file.writeAsBytes(response.bodyBytes);
      }
    } catch (e) {
      print(e);
    }
  }
}

class ReasonButtonWidget extends StatelessWidget {
  final Function onTap;
  final String title;
  final String subtitle;

  const ReasonButtonWidget({Key key, this.onTap, this.title, this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
        decoration: BoxDecoration(
          color: kPrimaryColor,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[800].withOpacity(0.40),
              blurRadius: 20,
              offset: Offset(5, 10),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            Text(subtitle)
          ],
        ),
      ),
    );
  }
}
