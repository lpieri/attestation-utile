import 'dart:io';

import 'package:attestation_utile/constants.dart';
import 'package:flutter/material.dart';

class CertificateListScreen extends StatelessWidget {
  final myDir = new Directory(kCertificateDirectory);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste des attestations'),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            children: list(),
          ),
        ),
      ),
    );
  }

  List<FileCardWidget> list() {
    List<FileCardWidget> widgets = [];

    myDir.listSync().forEach((FileSystemEntity file) {
      var matches = file.path.split(new RegExp(r"(?:[\/\.])+"));
      widgets.add(
        FileCardWidget(
          reason: matches[matches.length - 2].substring(9),
          generated: file.statSync().modified,
        ),
      );
    });

    return widgets;
  }
}

class FileCardWidget extends StatelessWidget {
  final String reason;
  final DateTime generated;

  const FileCardWidget({Key key, this.reason, this.generated}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 8),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
      decoration: BoxDecoration(
        color: kPrimaryColor,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[800].withOpacity(0.40),
            blurRadius: 20,
            offset: Offset(5, 10),
          ),
        ],
      ),
      child: Column(
        children: [
          Text(
            reason,
            style: Theme.of(context)
                .textTheme
                .headline5
                .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          Text(
            formatDateTime(generated),
            style: Theme.of(context)
                .textTheme
                .headline6
                .copyWith(color: Colors.white, fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  }
  
  String formatDateTime(DateTime dt) {
    return "${dt.day < 10 ? '0' : ''}${dt.day}/${dt.month < 10 ? '0' : ''}${dt.month}/${dt.year} ${dt.hour < 10 ? '0' : ''}${dt.hour}:${dt.minute < 10 ? '0' : ''}${dt.minute}";
  }
}
