import 'dart:convert';

import 'package:attestation_utile/constants.dart';
import 'package:attestation_utile/models/user.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferencesScreen extends StatefulWidget {
  @override
  _UserPreferencesScreenState createState() => _UserPreferencesScreenState();
}

class _UserPreferencesScreenState extends State<UserPreferencesScreen> {
  final _formKey = GlobalKey<FormState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String _firstname;
  String _lastname;
  DateTime _birthdate;
  String _birthplace;
  String _address;
  String _city;
  int _zipcode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Identité'),
        elevation: 0,
      ),
      body: Form(
        key: _formKey,
        child: Scrollbar(
          child: Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ...buildFormFields(),
                  buildFormSubmitButton(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildTextFormField(
      String hintText, String errorText, Function onSaved) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: TextFormField(
        style: TextStyle(color: kPrimaryColor),
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(color: kPlaceholderColor),
          filled: true,
          fillColor: Colors.blue[50],
          contentPadding: EdgeInsets.all(20),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(24),
          ),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return errorText;
          }
          return null;
        },
        onSaved: onSaved,
      ),
    );
  }

  onSubmit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      var _user = User(_firstname, _lastname, _birthdate, _birthplace, _address,
          _city, _zipcode);
      saveUser(_user);
    }
  }

  saveUser(User user) async {
    final SharedPreferences prefs = await _prefs;
    final String serializedUser = jsonEncode(user.toJson());

    prefs.setString('user', serializedUser);
    Navigator.pushNamed(context, '/');
  }

  Widget buildFormSubmitButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: FlatButton(
        onPressed: () => onSubmit(),
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        color: Colors.indigo[900],
        minWidth: double.infinity,
        child: Text(
          'Enregistrer les informations'.toUpperCase(),
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  List<Widget> buildFormFields() {
    return [
      buildTextFormField(
        'Prénom',
        'Veuillez indiquer votre prénom',
        (value) => setState(() => _firstname = value),
      ),
      buildTextFormField(
        'Nom',
        'Veuillez indiquer votre nom',
        (value) => setState(() => _lastname = value),
      ),
      buildTextFormField(
        'Date de naissance',
        'Veuillez indiquer votre date de naissance',
        (value) => setState(() => _birthdate = DateTime.tryParse(value)),
      ),
      buildTextFormField(
        'Lieu de naissance',
        'Veuillez indiquer votre lieu de naissance',
        (value) => setState(() => _birthplace = value),
      ),
      buildTextFormField(
        'Adresse',
        'Veuillez indiquer votre adresse',
        (value) => setState(() => _address = value),
      ),
      buildTextFormField(
        'Ville',
        'Veuillez indiquer votre ville',
        (value) => setState(() => _city = value),
      ),
      buildTextFormField(
        'Code postal',
        'Veuillez indiquer votre code postal',
        (value) => setState(() => _zipcode = int.parse(value)),
      ),
    ];
  }
}
