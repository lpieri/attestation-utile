# Attestation utile

Attestation utile est une application mobile développée avec Flutter pour générer des attestations de sortie.

|     |     |
| --- | --- |
|![identité](doc/img/user_preferences.png "Identité") | ![motifs](doc/img/authorization_reason.png "Motifs de déplacement")|

## Dépendances

- [http](https://pub.dev/packages/http)
- [shared_preferences](https://pub.dev/packages/shared_preferences)
